package PackSemaforo;

public class Semaforo {

    String colore = "verde";
    int timer = 50,timerAttuale;


    public Semaforo(int tick){
        this.timer = tick;
    }

    public void timer (){
        while (true) {
            if (timerAttuale < 0) {
                changeColor();
                System.out.println(toString());
                timerAttuale = timer;
            }
            timerAttuale--;
        }
    }

    public String toString(){
        return colore;
    }

    public void changeColor(){
        switch (colore){
            case "verde":
                colore = "giallo";
                break;
            case "giallo":
                colore = "rosso";
                break;
            case "rosso":
                colore = "verde";
                break;
        }
    }


}
